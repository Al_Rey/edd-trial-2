# EDD trial 2

My original EDD project stopped working out of nowhere so I am remaking it here to try and find what went wrong

**REPOS:**

*EDD* - when the original code stopped working this file was made to hold a remake of the old code to locate any errors. However the code reached a point where the serial monitor started printing gibberish

*EDD-alternate* - This contains alternate versions of the code. The code implements the website in a different way that is said to take up less storage. The working copy of the code is here

*Reference* - anything that was referenced or used in the final code is here including the website HTML code and any programs made to test logic or certain features seperatly from the rest of the code.

*Ideas* - any capabilities that we wanted to impement but didn't make into the final version of the code