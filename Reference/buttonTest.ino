int buttonPin = 16;
int ledPin = 5;
int buttonStatus = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  buttonStatus = digitalRead(buttonPin);
  
  if(buttonStatus == HIGH){
    digitalWrite(ledPin, HIGH);
    Serial.println("button was pressed");
  }else{
    digitalWrite(ledPin, LOW);
    //Serial.print("button was not pressed");
  }
}
