// Load Wi-Fi library
#include <ESP8266WiFi.h>


// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
//String output5State = "off";
//String output4State = "off";

// Assign variables to GPIO pins
const int greenLED = 15;
const int yellowLED = 2;//was 2
const int requestBtn = 5;
const int cancelBtn = 12;//DON'T USE 0 AS AN INPUT (D3)

int person1, person2, person3, person4, person5, person6;

// Set web server port number to 80
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
   //GREEN LED - request sent
  pinMode(greenLED, OUTPUT);
  
  //YELLOW LED - cancel light (teacher cancels request / student cancels request
  pinMode(yellowLED, OUTPUT);

// Replace with your network credentials
const char* ssid     = "AndroidAP5B11";
const char* password = "rykr8399";


  //CANCEL BUTTON
  pinMode(cancelBtn, INPUT);

  //REQUEST BUTTON
  pinMode(requestBtn, INPUT);

  digitalWrite(greenLED, LOW);
  digitalWrite(yellowLED, LOW);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
             if(digitalRead(requestBtn)){
                digitalWrite(greenLED, HIGH);
                //digitalWrite(yellowLED, HIGH);
             }else{
                digitalWrite(greenLED, LOW);
                //digitalWrite(yellowLED, LOW);
             }

             if(digitalRead(cancelBtn)){
                digitalWrite(yellowLED, HIGH);
             }else{
                digitalWrite(yellowLED, LOW);
             }

            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style> body { background-color: lightblue;}");
            client.println("h1{ text-align:center; text-decoration: underline; border-style: outset;}");
            client.println("#title {color: black;} </style></head>");

            client.println("<sript>");
            client.println("function myFunction1() {var studentone = document.getElementById(\"mytext1\").value; document.getElementById(\"01\").innerHTML = studentone;}");
            client.println("function myFunction2() {var studenttwo = document.getElementById(\"mytext2\").value; document.getElementById(\"02\").innerHTML = studenttwo;}");
            client.println("function myFunction3() {var studentthree = document.getElementById(\"mytext3\").value; document.getElementById(\"03\").innerHTML = studentthree;}");
            client.println("function myFunction4() {var studentfour = document.getElementById(\"mytext4\").value; document.getElementById(\"04\").innerHTML = studentfour;}");
            client.println("function myFunction1() {var studentfive = document.getElementById(\"mytext5\").value; document.getElementById(\"05\").innerHTML = studentfive;}");
            client.println("function myFunction1() {var studentsix = document.getElementById(\"mytext6\").value; document.getElementById(\"06\").innerHTML = studentsix;}");
            client.println("</script>");
            
            // Web Page Heading
            client.println("<body><h1>Welcome Back!</h1>");

            client.println("<table style=\"width:100%\">");
            client.println("<tr id=\"title\">");
            client.println("th>Student</th> <th>Help Requested</th> <th>Order of Requests</th> <th>Request Canceled</th> <th>Cancel Request</th>");
            client.println("</tr>");
            
            client.println("<tr>");
            client.println("<td id=\"01\">01</td> <td></td> <td></td> <td></td> <td><button>Cancel</button></td>");
            client.println("</tr>");
            
            client.println("<tr>");
            client.println("<td id=\"02\">02</td> <td></td> <td></td> <td></td> <td><button>Cancel</button></td>");
            client.println("</tr>");

            client.println("<tr>");
            client.println("<td id=\"03\">03</td> <td></td> <td></td> <td></td> <td><button>Cancel</button></td>");
            client.println("</tr>");

            client.println("<tr>");
            client.println("<td id=\"04\">04</td> <td></td> <td></td> <td></td> <td><button>Cancel</button></td>");
            client.println("</tr>");

            client.println("<tr>");
            client.println("<td id=\"05\">05</td> <td></td> <td></td> <td></td> <td><button>Cancel</button></td>");
            client.println("</tr>");

            client.println("<tr>");
            client.println("<td id=\"06\">06</td> <td></td> <td></td> <td></td> <td><button>Cancel</button></td>");
            client.println("</tr>");
            
//            // Display current state, and ON/OFF buttons for GPIO 5  
//            client.println("<p>GPIO 5 - State \" + output5State + \"</p>");
//            // If the output5State is off, it displays the ON button       
//            if (output5State=="off") {
//              client.println("<p><a href=\"/5/on\"><button class=\"button\">ON</button></a></p>");
//            } else {
//              client.println("<p><a href=\"/5/off\"><button class=\"button button2\">OFF</button></a></p>");
//            } 
//               
//            // Display current state, and ON/OFF buttons for GPIO 4  
//            client.println("<p>GPIO 4 - State " + output4State + "</p>");
//            // If the output4State is off, it displays the ON button       
//            if (output4State=="off") {
//              client.println("<p><a href=\"/4/on\"><button class=\"button\">ON</button></a></p>");
//            } else {
//              client.println("<p><a href=\"/4/off\"><button class=\"button button2\">OFF</button></a></p>");
//            }
//            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

//public void checkOrder(int pin){
//  
//}
