#include <ESP8266WiFi.h>

const char* ssid = "majimbe_EXT";
const char* password = "Apolinaria";

boolean helpOn1 = false, cancelOn1 = false;
boolean helpOn2 = false, cancelOn2 = false;
boolean allZerio = false;

int i = 0, maxNum, oldPlace;

const int greenLED = 15;//was 2
const int yellowLED = 2;//was 15

const int redLED2 = 5;
const int yellowLED2 = 3;
const int requestBtn1 = 4;
const int cancelBtn1 = 14;
const int requestBtn2= 12;
const int cancelBtn2 = 13;

String text[] = {"", "", "", "", "", ""};
int counter[] = {0,0,0,0,0,0};
int order[] = {0,0,0,0,0,0};

WiFiServer server(80);


void setup(){
  Serial.begin(115200);
  Serial.println();

  pinMode(greenLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
  pinMode(requestBtn1, INPUT);
  pinMode(requestBtn2, INPUT);
  pinMode(cancelBtn1, INPUT);
  pinMode(cancelBtn2, INPUT);
  pinMode(redLED2, OUTPUT);
  pinMode(yellowLED2, OUTPUT);  
  
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");

  server.begin();
  Serial.printf("Web server started, open %s in a web browser\n", WiFi.localIP().toString().c_str());
}


// prepare a web page to be send to a client (web browser)
String prepareHtmlPage(){
  String htmlPage =
     String("HTTP/1.1 200 OK\r\n") +
            "Content-Type: text/html\r\n" +
            //"Connection: close\r\n" +  // the connection will be closed after completion of the response
            //"Refresh: 1\r\n" +  // refresh the page automatically every 5 sec
            "\r\n" +
            "<!DOCTYPE HTML>" +
            "<html>" +
            "<head>" +
            "<meta http-equiv=\"refresh\" content=\"2\">" +
            "<style>" +
              "body { background-color:lightblue;}" +
              "h1 { text-align: center; text-decoration: underline; border-style: outset;}" +
              "table {text-align: center; border: solid 10px;}" +
              "th {border: solid 1px; border-collapse: collapse; padding: 2px;}" +
              "td {border: solid 1px;}" +
              "#cancel {width: fit-content;}" +
            "</style>" +
            "</head>" +
            "<body>" +
            "<h1>Welcome Back!</h1>" +
            "<p style=\"text-align:center;\">Below is a chart that includes your student's names and their requests.</p>" +
            "<table style=\"width:100%\">" +
              "<tr id=\"title\">" +
                "<th>Student</th>" +
                "<th>Help Requested</th>" +
                "<th>Order of Requests</th>" +
                "<th>Times requested</th>" +
                "<th>Request Canceled</th>" +
                "<th>Cancel Request</th>" +
              "</tr>" +
                "<tr>" +
                "<td id=\"01\">01</td>" +
                "<td>" + text[0] + "</td>" +
                "<td>" + order[0] + "</td>" +
                "<td class = \"counter\">" + counter[0] + "</td>" +
                "<td>" + text[0] + "</td>" +
                "<td><button id=\"cancel\">Cancel</button></td>" +
              "</tr>" +
              "<tr>" +
                "<td id=\"01\">02</td>" +
                "<td>" + text[1] + "</td>" +
                "<td>" + order[1] + "</td>" +
                "<td class = \"counter\">" + counter[1] + "</td>" +
                "<td>" + text[1] + "</td>" +
                "<td><button id=\"cancel\">Cancel</button></td>" +
              "</tr>" +
            "</table>" +
            "Analog input:  " + String(digitalRead(4)) +
            "</body>" +
            "</html>" +
            "\r\n";
  return htmlPage;
}


void loop(){
  WiFiClient client = server.available();
  // wait for a client (web browser) to connect
  if (client)
  {
    Serial.println("\n[Client connected]");
//    client.println(prepareHtmlPage());
    while (client.connected())
    {
      // read line by line what the client (web browser) is requesting
      if (client.available())
      {
        String line = client.readStringUntil('\r');
        Serial.print(line);
        // wait for end of client's request, that is marked with an empty line
        if (line.length() == 1 && line[0] == '\n'){
          if(!helpOn1 && digitalRead(requestBtn1)){//Student 1 request button
            helpPressed(1);
            Serial.println("Help button pressed");
            Serial.println(helpOn1);
            reorder(1);
          } else if(helpOn1 && digitalRead(cancelBtn1)){//Student 1 cancel button
            cancelPressed(1);
            Serial.println("Cancel button pressed");
            reorder(1);
          }else{//no buttons have been pressed form either student
            nothingPressed(1);
            Serial.println("Nothing was pressed (student 1)");
            
            Serial.print("Help1 On: ");
            Serial.println(helpOn1);
            
            Serial.print("Cancel1 On: ");
            Serial.println(cancelOn1);
            
            Serial.println("");
          }
          
          if(!helpOn2 && digitalRead(requestBtn2)){//Student 2 request button
            helpPressed(2);
            Serial.println("help button 2 pressed");
            Serial.println(helpOn2);
            reorder(2);
          }else if(helpOn2 && digitalRead(cancelBtn2)){//Student 2 cancel button
            cancelPressed(2);
            Serial.println("Cancel button 2 pressed");
            reorder(2);
          }else{//no buttons have been pressed form either student
            nothingPressed(2);
            Serial.println("Nothing was pressed (student 2)");
//            Serial.println("Help On: " + helpOn1);
//            Serial.println("Cancel On: " + cancelOn1);
//            Serial.println("");
            Serial.print("Help2 On: ");
            Serial.println(helpOn2);
            
            Serial.print("Cancel2 On: ");
            Serial.println(cancelOn2);
          }
           
          client.println(prepareHtmlPage());
          break;
        }
      }
    }
    delay(1); // give the web browser time to receive the data

    // close the connection:
//    client.stop();
//    Serial.println("[Client disonnected]");
  }
}

void helpPressed(int studentNum){
  text[studentNum-1] = "HELP REQUESTED";
//  digitalWrite(greenLED, HIGH);
  counter[studentNum-1]++;
  switch(studentNum){
    case 1:
      cancelOn1 = false;
      helpOn1 = true;
      digitalWrite(greenLED, HIGH);
    case 2:
      cancelOn2 = false;
      helpOn2 = true;
      digitalWrite(yellowLED2, HIGH);
    default:
      break;
  }
}

void cancelPressed(int studentNum){
//  text[studentNum-1] = "REQUEST CANCELLED";
//  digitalWrite(yellowLED, HIGH);
//  digitalWrite(greenLED, LOW);
  switch(studentNum){
    case 1:
      digitalWrite(yellowLED, HIGH);
      digitalWrite(greenLED, LOW);
      if(helpOn1)
        text[0] = "REQUEST CANCELLED";
      cancelOn1 = true;
      helpOn1 = false;
      break;
    case 2:
      digitalWrite(redLED2, HIGH);
      digitalWrite(yellowLED2, LOW);
      if(helpOn2)
        text[1] = "REQUEST CANCELLED";
      cancelOn2 = true;
      helpOn2 = false;
      break;
    default:
      break;
  }
}

void nothingPressed(int studentNum){
  if(cancelOn1 || cancelOn2){
    delay(.5);
//    digitalWrite(yellowLED, LOW);
//    text[studentNum-1] = "";
    if(cancelOn1){
      text[0] = "";
      cancelOn1 = false;
      digitalWrite(yellowLED, LOW);
    }
    
    if(cancelOn2){
        text[1] = "";
        cancelOn2 = false;
        digitalWrite(redLED2, LOW);
    }    
  }
}

void reorder(int studentNum){
    i = studentNum - 1;
    maxNum = 0;

    if(order[i] == 0){
        for(int x = 0; x < 6; x++){
            if(maxNum < order[x]){
                maxNum = order[x];
            }
        }
        order[i] = ++maxNum;
    }else{
        for(int x = 0; x < 6; x++){
            if(order[x] > order[i]){
                order[x]--;
            }
        }
        order[i] = 0;
    }
}
