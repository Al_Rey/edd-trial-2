#include <ESP8266WiFi.h>

const char* ssid = "majimbe_EXT";
const char* password = "Apolinaria";

boolean helpOn1 = false, cancelOn1 = false, helpOn2 = false, cancelOn2 = false;

const int greenLED = 2;//was 15
const int yellowLED = 15;//was 2
const int requestBtn1 = 4;
const int cancelBtn1 = 14;

String text[6] = {"", "", "", "", "", ""};
int counter[6] = {0,0,0,0,0,0};
int order[6] = {0,0,0,0,0,0};

WiFiServer server(80);

void setup()
{
  Serial.begin(115200);
  Serial.println();

  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");

  server.begin();
  Serial.printf("Web server started, open %s in a web browser\n", WiFi.localIP().toString().c_str());
}


// prepare a web page to be send to a client (web browser)
String prepareHtmlPage()
{
  String htmlPage =
     String("HTTP/1.1 200 OK\r\n") +
            "Content-Type: text/html\r\n" +
            //"Connection: close\r\n" +  // the connection will be closed after completion of the response
            //"Refresh: 1\r\n" +  // refresh the page automatically every 5 sec
            "\r\n" +
            "<!DOCTYPE HTML>" +
            "<html>" +
            "<head>" +
            "<meta http-equiv=\"refresh\" content=\"5\">" +
            "<style>" +
              "body { background-color:lightblue;}" +
              "h1 { text-align: center; text-decoration: underline; border-style: outset;}" +
              "table {text-align: center; border: solid 10px;}" +
              "th {border: solid 1px; border-collapse: collapse; padding: 2px;}" +
              "td {border: solid 1px;}" +
              "#cancel {width: fit-content;}" +
            "</style>" +
            "</head>" +
            "<body>" +
            "<h1>Welcome Back!</h1>" +
            "<p style=\"text-align:center;\">Below is a chart that includes your student's names and their requests.</p>" +
            "<table style=\"width:100%\">" +
              "<tr id=\"title\">" +
                "<th>Student</th>" +
                "<th>Help Requested</th>" +
                "<th>Order of Requests</th>" +
                "<th>Times requested</th>" +
                "<th>Request Canceled</th>" +
                "<th>Cancel Request</th>" +
              "</tr>" +
                "<tr>" +
                "<td id=\"01\">01</td>" +
                "<td>" + text[0] + "</td>" +
                "<td>" + order[0] + "</td>" +
                "<td class = \"counter\">" + counter[0] + "</td>" +
                "<td>" + text[0] + "</td>" +
                "<td><button id=\"cancel\">Cancel</button></td>" +
              "</tr>" +
            "</table>" +
            "Analog input:  " + String(digitalRead(4)) +
            "</body>" +
            "</html>" +
            "\r\n";
  return htmlPage;
}


void loop()
{
  WiFiClient client = server.available();
  // wait for a client (web browser) to connect
  if (client)
  {
    Serial.println("\n[Client connected]");
    String line = client.readStringUntil('\r');
    Serial.print(line);
        // wait for end of client's request, that is marked with an empty line
    if (line.length() == 1 && line[0] == '\n')
    {
      client.println(prepareHtmlPage());
    }
    while (client.connected())
    {
      // read line by line what the client (web browser) is requesting
      if (client.available())
      {
          if(!helpOn1 && digitalRead(requestBtn1)){
            helpPressed(1);
            Serial.println("Help button pressed");
            Serial.println(helpOn1);
          } else if(helpOn1 && digitalRead(cancelBtn1)){
            cancelPressed(1);
            Serial.println("Cancel button pressed");
          }else{
            nothingPressed(1);
//            Serial.println("Nothing was pressed");
//            Serial.println("Help On: " + helpOn1);
//            Serial.println("Cancel On: " + cancelOn1);
          }
           
//          client.println(prepareHtmlPage());
//          break;
        
      }
    }
    delay(1); // give the web browser time to receive the data

    // close the connection:
//    client.stop();
    Serial.println("[Client disonnected]");
  }
}

void helpPressed(int studentNum){
  text[studentNum-1] = "HELP REQUESTED";
  digitalWrite(greenLED, HIGH);
  counter[studentNum-1]++;
  switch(studentNum){
    case 1:
      cancelOn1 = false;
      helpOn1 = true;
    case 2:
      cancelOn2 = false;
      helpOn2 = true;
    default:
      break;
  }
}

void cancelPressed(int studentNum){
  text[studentNum-1] = "REQUEST CANCELLED";
  digitalWrite(yellowLED, HIGH);
  digitalWrite(greenLED, LOW);
  switch(studentNum){
    case 1:
      cancelOn1 = true;
      helpOn1 = false;
    case 2:
      cancelOn2 = true;
      helpOn2 = false;
    default:
      break;
  }
}

void nothingPressed(int studentNum){
  if(cancelOn1 || cancelOn2){
    delay(.5);
    digitalWrite(yellowLED, LOW);
    text[studentNum-1] = "";
  }
}
