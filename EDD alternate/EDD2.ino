/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

// Load Wi-Fi library
#include <ESP8266WiFi.h>

// Replace with your network credentials
const char* ssid     = "AndroidAP5B11";
const char* password = "rykr8399";

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Assign output and input variables to GPIO pins
const int greenLED = 15;
const int yellowLED = 2;
const int requestBtn1 = 4;
const int cancelBtn1 = 14;

int indexes, maxNum, oldPlace;
boolean helpOn = false, cancelOn = false;

int order[6] = {0,0,0,0,0,0};
int counter[6] = {0,0,0,0,0,0};
String text[6] = {"","","","","",""};

void helpPressed(int studentNum);
void cancelPressed(int studentNum);

int i = 0;

void setup() {
  Serial.begin(115200);
  // Initialize the output variables as outputs
  pinMode(requestBtn1, INPUT);
  pinMode(cancelBtn1, INPUT);
  pinMode(greenLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
  // Set outputs to LOW
  digitalWrite(greenLED, LOW);
  digitalWrite(yellowLED, LOW);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (text[0].equals("") && !helpOn){
                if(digitalRead(requestBtn1)){
                    digitalWrite(greenLED, HIGH);
                    helpPressed(1);
                    counter[0]++;
                    Serial.println("Request button was pressed");
                    helpOn = true;
                    while(digitalRead(requestBtn1)){}
                }
            }


            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            
//            // CSS to style the website
//            client.println("<style>");
//            client.println("body { background-color: lightblue; color: black;}");
//            client.println("h1 { text-align: center; text-decoration: underline; border-style: outset;}");
//            client.println("#title {color: black;}");
//            client.println("table {text-align: center; border: solid 10px;}");
//            client.println("th {border: solid 1px; border-collapse: collapse; padding: 2px;}");
//            client.println("td {border: solid 1px;}");
//            client.println("#cancel {width: fit-content;}");
//            client.println("</style>");
            
            client.println("</head>");
            
            // Web Page Heading
            client.println("<body>");
            client.println("<h1>Welcome Back!</h1>");
            
            // Display instructions for website  
            client.println("<p style=\"text-align:center;\">Below is a chart that includes your student's names and their requests.</p>");
           
            // creates the table that is displayed
            client.println("<table style=\"width:100%\">");
            client.println("<tr id=\"title\">");
            client.println("<th>Student</th>");
            client.println("<th>Help Requested</th> ");
            client.println("<th>Order of Requests</th>");
            client.println("<th>Times requested</th>");
            client.println("<th>Request Canceled</th>");
            client.println("<th>Cancel Request</th>");
            client.println("</tr>");

            //student 1~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            client.println("<tr>");
            client.println("<td id=\"01\">01</td>");
            client.println("<td>");
//            client.println(text[0]);//HELP BUTTON PRESSED
            if(digitalRead(requestBtn1){
              i++;
            }
            client.println(i)
            client.println("</td>");
            
            client.println("<td>");//dispays order
            client.println(order[0]);
            client.println("</td>");
            
            client.println("<td class = \"count\">");
            client.println(counter[0]);
            client.println("</td>");
            
            client.println("<td>");
            if(digitalRead(cancelBtn1) && helpOn){
                digitalWrite(yellowLED, HIGH);
                digitalWrite(greenLED, LOW);
                client.println("REQUEST CANCELED");
                cancelPressed(1);
                Serial.println("Cancel Button Pressed");
                delay(5000);
                while(digitalRead(cancelBtn1)){}
                digitalWrite(yellowLED, LOW);
            }
            client.println("</td>");
            
            client.println("<td><button id=\"cancel\">Cancel</button></td>");
            
            client.println("</tr>");
            client.println("</table>");
            
           
            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

void helpPressed(int studentNum){
  text[studentNum-1] = "HELP REQUESTED";
}

void cancelPressed(int studentNum){
  text[studentNum-1] = "";
}
